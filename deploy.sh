avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega2560 -c -o hello_world.o hello_world.c
avr-gcc -mmcu=atmega2560 hello_world.o -o hello_world.elf
avr-objcopy -O ihex -R .eeprom hello_world.elf hello_world.hex
avrdude -F -V -cwiring -patmega2560 -P/dev/ttyACM0 -b115200 -Uflash:w:hello_world.hex
