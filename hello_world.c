/*
* This module is written for the Arduino Mega2560 and any HD4660 compliant 16-pin LCD screen.
* The module takes a string and displays it onto the LCD by programming the LCD using its I/O pins.
* Pin connections were:
* 	RS -> PB0
* 	E -> PB2
* 	D0-7 -> PL0-7
*/
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>

#define E_DELAY_MS (50)
#define CLEAR_SCREEN (0x01)
#define TWO_LINE_8_BIT (0x38)
#define SCREEN_ON_CURSOR_ON (0x0e)


void send_command(unsigned char command)
{
	PORTL = command;
	// Set RS to 0 - instruction
	PORTB &= ~_BV(PORTB0);
	
	// Set E to 1 - start write of the command
	PORTB |= _BV(PORTB2);
	_delay_ms(E_DELAY_MS);
	// Set E to 0 - finish write of command
	PORTB &= ~_BV(PORTB2);

	// zero data bits
	PORTL = 0;
}


void send_char(unsigned char data)
{
	PORTL = data;
	// Set RS to 1 - data
	PORTB |= _BV(PORTB0);
	
	// Set E to 1 - start write of the data
	PORTB |= _BV(PORTB2);
	_delay_ms(E_DELAY_MS);
	// Set E to 0 - finish write of data
	PORTB &= ~_BV(PORTB2);

	// zero data bits
	PORTL = 0;
}

void init_screen()
{
	send_command(CLEAR_SCREEN);
	send_command(TWO_LINE_8_BIT);
	send_command(SCREEN_ON_CURSOR_ON);
}

void send_str(const char *str)
{
	int i;
	size_t len = strlen(str);
	for (i = 0; i < len; i++) {
		send_char((unsigned char)str[i]);
	}
}

int main(void) {
	// Setting all pins of PORTB and PORTL to output
	DDRB = 0xff;
	DDRL = 0xff;

	init_screen();
	const char *str = "LCDs are 1337 :)";
	send_str(str);
}
